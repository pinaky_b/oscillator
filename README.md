=== OSCILLATOR ===

This Visual C++ project contains a library with the namespace 'sdof'
that contains an abstract oscillator class.

The goal is to be able to simulate a single-degree-of-freedom oscillator.

The library contains two concrete implementations of the abstract oscillator:
one a linear oscillator and the other a non-linear duffing oscillator.

Each implementation is equipped with a builder class that allows its construction.
In order to construct the object, you need to specify the governing parameters
of the differential equation as well as initial conditions.

Each implementation is also equipped with a method that allows you to perform 
numerical integration over time in order to determine the trajectory of the
oscillator. Currently, a constant time-step and number of steps need to be 
specified. This uses C++ BOOST's ODEINT packacge.