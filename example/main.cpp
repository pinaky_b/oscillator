// examplelinearoscillator.cpp : Defines the entry point for the console application.
//
#include "inputtoken.h"
#include "linearoscillator.h"
#include "duffingoscillator.h"
#include <string>
#include <iostream>
#include <fstream>

int main()
{
	std::string output_filename(sdof::InputToken<std::string>("Enter a name for the output file: ").Execute().GetValue());

	sdof::TimeInformationConstStep t = sdof::TimeInformationConstStep::TimeInformationConstStepBuilder()
		.SetDt(sdof::InputToken<double>("Enter a value for the time-increment dt: ").Execute().GetValue())
		.SetNumTimeSteps(sdof::InputToken<uint64_t>("Enter a value for the number of time-steps num_timesteps: ").Execute().GetValue())
		.Build();

	// Get oscillator input data
	const double omega_linear = sdof::InputToken<double>("Enter a value for oscillator natural frequency omega: ").Execute().GetValue();
	const double zeta_linear = sdof::InputToken<double>("Enter a value for oscillator damping ratio zeta: ").Execute().GetValue();
	const double accamp_linear = sdof::InputToken<double>("Enter a value for oscillator forcing acceleration amplitude accamp: ").Execute().GetValue();
	const double accfrq_linear = sdof::InputToken<double>("Enter a value for oscillator forcing frequency accfrq: ").Execute().GetValue();
	const double alpha_duffing = sdof::InputToken<double>("Enter a value for oscillator non-linear coefficient: ").Execute().GetValue();
	const double xinit = sdof::InputToken<double>("Enter a value for oscillator initial position xinit: ").Execute().GetValue();
	const double vinit = sdof::InputToken<double>("Enter a value for oscillator initial velocty vinit: ").Execute().GetValue();
	// Infer equivalent duffing parameters
	const double beta_duffing = omega_linear * omega_linear;
	const double delta_duffing = 2.0 * omega_linear * zeta_linear;
	
	// Construct and run the linear oscillator
	sdof::LinearOscillator linear_oscillator = sdof::LinearOscillator::LinearOscillatorBuilder()
		.SetOmega(omega_linear).SetZeta(zeta_linear).SetAccAmp(accamp_linear).SetAccFrq(accfrq_linear).SetXInit(xinit).SetVInit(vinit).Build();
	linear_oscillator.Integrate(t);

	// Construct and run the duffing oscillator
	sdof::DuffingOscillator duffing_oscillator = sdof::DuffingOscillator::DuffingOscillatorBuilder()
		.SetAlpha(alpha_duffing).SetBeta(beta_duffing).SetDelta(delta_duffing).SetGamma(accamp_linear).SetOmega(accfrq_linear)
		.SetVInit(vinit).SetXInit(xinit).Build();
	duffing_oscillator.Integrate(t);

	// Print the trajectory out
	std::ofstream file(output_filename + "_linear.txt");
	file << "{" << std::endl;
	linear_oscillator.PrintTrajectoryJSON(file);
	file << "}" << std::endl;
	file.close();

	file.open(output_filename + "_duffing.txt");
	file << "{" << std::endl;
	duffing_oscillator.PrintTrajectoryJSON(file);
	file << "}" << std::endl;
	file.close();

	linear_oscillator.PrintParametersJSON(std::cout);
	duffing_oscillator.PrintParametersJSON(std::cout);

    return 0;
}

