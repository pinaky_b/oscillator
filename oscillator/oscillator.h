#pragma once
#include <vector>
#include <exception>

namespace sdof
{
	typedef std::vector<double> state_type;

	class Oscillator
	{
	public:
		virtual void operator()(state_type const &, state_type &, double) = 0;
	};
}