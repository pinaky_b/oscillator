#pragma once
#include <iostream>
#include <cstdint>
#include <exception>

namespace sdof
{
	class TimeInformationConstStep
	{
	private:
		uint64_t num_timesteps_;
		double dt_;
	public:
		class TimeInformationConstStepBuilder;
		//static const uint64_t default_num_timesteps;
		//static const double default_dt;
		//TimeInformationConstStep() : num_timesteps_(default_num_timesteps),  dt_(default_dt) {}
		//TimeInformationConstStep(uint64_t num_timesteps) : dt_(default_dt) { this->num_timesteps_ = num_timesteps; }
		//TimeInformationConstStep(double dt) : num_timesteps_(default_num_timesteps) { this->dt_ = dt; }

		TimeInformationConstStep(uint64_t num_timesteps, double dt) { this->num_timesteps_ = num_timesteps; this->dt_ = dt; }

		// Getter methods. I know, I know...
		double GetDt() const { return this->dt_; }
		uint64_t GetNumTimeSteps() const { return this->num_timesteps_; }
	};

	class TimeInformationConstStep::TimeInformationConstStepBuilder
	{
	private:
		uint64_t num_timesteps_;
		double dt_;
	public:
		static const uint64_t default_num_timesteps;
		static const double default_dt;

		TimeInformationConstStepBuilder() : num_timesteps_(default_num_timesteps), dt_(default_dt) {}

		TimeInformationConstStepBuilder& SetNumTimeSteps(const uint64_t num_timesteps) { this->num_timesteps_ = num_timesteps; return *this;  }
		TimeInformationConstStepBuilder& SetDt(const double dt) { this->dt_ = dt; return *this; }

		TimeInformationConstStep Build()
		{
			try
			{
				if (this->dt_ < 0.0) throw std::logic_error("Time step dt should not be negative.");
			}
			catch (std::logic_error &e)
			{
				std::cerr << e.what() << std::endl; 
				exit(1);
			}

			return TimeInformationConstStep(this->num_timesteps_, this->dt_);
		}
	};

}