#pragma once
#include "oscillator.h"
#include <iostream>
#include "timeinformationconststep.h"

// This class implements the state-derivative of a linear mechanical SDOF oscillator:
// x.. + 2 zeta omega x. + omega ^ 2 x = accamp * cos(accfrq t)

namespace sdof
{
	class DuffingOscillator : public Oscillator
	{
	private:
		std::vector<state_type> x_vec;
		std::vector<double> times;
		double omega_; // Forcing frequency
		double gamma_; // Forcing amplitude
		double alpha_; // Non-linear coefficient
		double beta_; // Linear stiffness coefficient
		double delta_; // Viscous damping coefficient
		double xinit_; // Initial displacement
		double vinit_; // Initial velocity
	public:
		class DuffingOscillatorBuilder;
		DuffingOscillator();
		DuffingOscillator(double omega, double gamma, double alpha, double beta, double delta, double xinit, double vinit) :
			omega_(omega), gamma_(gamma), alpha_(alpha), beta_(beta), delta_(delta), xinit_(xinit), vinit_(vinit) {}
		void Integrate(TimeInformationConstStep const &);
		void operator()(state_type const &, state_type &, double);
		void PrintParametersJSON(std::ostream &, std::string const &s = "duffingoscillatorparameters");
		void PrintTrajectoryJSON(std::ostream &);
		std::vector<double> const & GetTimes() { return this->times; }
		std::vector<state_type> const & GetXVec() { return this->x_vec; }
	};


	// Builder for the LinearOscillator class
	class DuffingOscillator::DuffingOscillatorBuilder
	{
	private:
		double omega_; // Forcing frequency
		double gamma_; // Forcing amplitude
		double alpha_; // Non-linear coefficient
		double beta_; // Linear stiffness coefficient
		double delta_; // Viscous damping coefficient
		double xinit_; // Initial displacement
		double vinit_; // Initial velocity
	public:
		static const double default_omega;
		static const double default_gamma;
		static const double default_alpha;
		static const double default_beta;
		static const double default_delta;
		static const double default_xinit;
		static const double default_vinit;

		DuffingOscillatorBuilder() : omega_(default_omega), gamma_(default_gamma),
			alpha_(default_alpha), beta_(default_beta), delta_(default_delta),
			xinit_(default_xinit), vinit_(default_vinit) {}

		DuffingOscillatorBuilder& SetOmega(const double omega) { this->omega_ = omega; return *this; }
		DuffingOscillatorBuilder& SetGamma(const double gamma) { this->gamma_ = gamma; return *this; }
		DuffingOscillatorBuilder& SetAlpha(const double alpha) { this->alpha_ = alpha; return *this; }
		DuffingOscillatorBuilder& SetBeta(const double beta) { this->beta_ = beta; return *this; }
		DuffingOscillatorBuilder& SetDelta(const double delta) { this->delta_= delta; return *this; }
		DuffingOscillatorBuilder& SetXInit(const double xinit) { this->xinit_ = xinit; return *this; }
		DuffingOscillatorBuilder& SetVInit(const double vinit) { this->vinit_ = vinit; return *this; }

		DuffingOscillator Build()
		{
			// Check if certain values are in the right range
			try
			{
				if (this->delta_ < 0.0) throw std::logic_error("Delta should be positive.");
				if (this->beta_ < 0.0) throw std::logic_error("Beta should be positive");
			}
			catch (std::logic_error& e)
			{
				std::cerr << e.what() << std::endl;
				exit(1);
			}

			return DuffingOscillator(this->omega_, this->gamma_, this->alpha_, this->beta_, this->delta_, this->xinit_, this->vinit_);
		}
	};
}