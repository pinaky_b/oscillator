#include "duffingoscillator.h"
#include "boost/numeric/odeint.hpp"
#include "observer.h"

namespace sdof
{

	const double DuffingOscillator::DuffingOscillatorBuilder::default_omega = 1.0;
	const double DuffingOscillator::DuffingOscillatorBuilder::default_gamma = 0.1;
	const double DuffingOscillator::DuffingOscillatorBuilder::default_alpha = 0.1;
	const double DuffingOscillator::DuffingOscillatorBuilder::default_beta = 0.1;
	const double DuffingOscillator::DuffingOscillatorBuilder::default_delta = 0.1;
	const double DuffingOscillator::DuffingOscillatorBuilder::default_xinit = 1.0;
	const double DuffingOscillator::DuffingOscillatorBuilder::default_vinit = 0.0;

	DuffingOscillator::DuffingOscillator()
	{
		this->omega_ = DuffingOscillator::DuffingOscillatorBuilder::default_omega;
		this->gamma_ = DuffingOscillator::DuffingOscillatorBuilder::default_gamma;
		this->alpha_ = DuffingOscillator::DuffingOscillatorBuilder::default_alpha;
		this->beta_ = DuffingOscillator::DuffingOscillatorBuilder::default_beta;
		this->delta_ = DuffingOscillator::DuffingOscillatorBuilder::default_delta;
		this->xinit_ = DuffingOscillator::DuffingOscillatorBuilder::default_xinit;
		this->vinit_ = DuffingOscillator::DuffingOscillatorBuilder::default_vinit;
	}

	void DuffingOscillator::Integrate(TimeInformationConstStep const &t)
	{

		const double dt = t.GetDt();
		const uint64_t num_timesteps = t.GetNumTimeSteps();
		const double tbegin = 0.0;
		const double tfinal = ((double)num_timesteps - 1.0) * dt;


		// Populate the initial state
		state_type initial_state(3);
		initial_state[0] = this->xinit_;
		initial_state[1] = this->vinit_;
		initial_state[2] = 0.0;

		// Reserve memory
		this->times.reserve(num_timesteps);
		this->x_vec.reserve(num_timesteps);

		// Perform the integration
		boost::numeric::odeint::runge_kutta4<state_type> stepper;
		size_t steps = boost::numeric::odeint::integrate_const(stepper, *this, initial_state, tbegin, tfinal, dt, Observer(this->x_vec, this->times));

	}

	void DuffingOscillator::operator()(state_type const &x, state_type &dxdt, double t)
	{
		dxdt[0] = x[1];
		dxdt[1] = -this->alpha_ * pow(x[0], 3) - this->beta_ * x[0] - this->delta_* x[1] + this->gamma_* cos(x[2]);
		dxdt[2] = this->omega_;
	}

	void DuffingOscillator::PrintTrajectoryJSON(std::ostream &os)
	{
		uint64_t i, n;
		os << "\t\"trajectory\": [" << std::endl;

		try
		{
			std::string err_message("Oscillator times and x_vec have different sizes.Problem with integration / observation routine");
			if (!(this->times.size() == this->x_vec.size())) throw std::logic_error(err_message);
		}
		catch (std::exception &e)
		{
			std::cerr << e.what() << std::endl;
			exit(1);
		}

		for (n = 0; n < this->times.size() - 1; n++)
		{
			os << "\t\t[";
			os << times[n] << ",";
			for (i = 0; i < x_vec[n].size() - 1; i++)
				os << x_vec[n][i] << ",";
			os << x_vec[n][i];
			os << "]," << std::endl;
		}
		os << "\t\t[";
		os << times[n] << ",";
		for (i = 0; i < x_vec[n].size() - 1; i++)
			os << x_vec[n][i] << ",";
		os << x_vec[n][i];
		os << "]" << std::endl;

		os << "\t]" << std::endl;
	}

	void DuffingOscillator::PrintParametersJSON(std::ostream &os, std::string const &s)
	{

		os << "\t\"" + s + "\": " << std::endl;
		os << "\t{" << std::endl;
		os << "\t\t\"omega\": " << this->omega_;
		os << ", \"gamma\": " << this->gamma_ << ", " << std::endl;
		os << "\t\t\"alpha\": " << this->alpha_;
		os << ", \"beta\": " << this->beta_;
		os << ", \"delta\": " << this->delta_ << ", " << std::endl;
		os << "\t\t \"xinit\": " << this->xinit_;
		os << ", \"vinit\": " << this->vinit_ << std::endl;
		os << "\t}" << std::endl;
	}
}