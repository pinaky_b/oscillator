#pragma once
#include "oscillator.h"
#include <iostream>
#include "timeinformationconststep.h"

// This class implements the state-derivative of a linear mechanical SDOF oscillator:
// x.. + 2 zeta omega x. + omega ^ 2 x = accamp * cos(accfrq t)

namespace sdof
{
	class LinearOscillator : public Oscillator
	{
	private:
		std::vector<state_type> x_vec;
		std::vector<double> times;
		double omega_; // Natural frequency
		double zeta_; // Damping
		double accamp_; // Acceleration amplitude
		double accfrq_; // Acceleration frequency
		double xinit_; // Initial displacement
		double vinit_; // Initial velocity
	public:
		class LinearOscillatorBuilder;
		LinearOscillator();
		LinearOscillator(double omega, double zeta, double accamp, double accfrq, double xinit, double vinit) :
			omega_(omega), zeta_(zeta), accamp_(accamp), accfrq_(accfrq), xinit_(xinit), vinit_(vinit) {}
		void Integrate(TimeInformationConstStep const &);
		void operator()(state_type const &, state_type &, double);
		void PrintParametersJSON(std::ostream &, std::string const &s = "linearoscillatorparameters");
		void PrintTrajectoryJSON(std::ostream &);
		std::vector<double> const & GetTimes() { return this->times; }
		std::vector<state_type> const & GetXVec() { return this->x_vec; }
		
	};


	// Builder for the LinearOscillator class
	class LinearOscillator::LinearOscillatorBuilder
	{
	private:
		double omega_; // Natural frequency
		double zeta_; // Damping
		double accamp_; // Acceleration amplitude
		double accfrq_; // Acceleration frequency
		double xinit_; // Initial displacement
		double vinit_; // Initial velocity
	public:
		static const double default_omega;
		static const double default_zeta;
		static const double default_accamp;
		static const double default_accfrq;
		static const double default_xinit;
		static const double default_vinit;

		LinearOscillatorBuilder() : omega_(default_omega), zeta_(default_zeta), 
			accamp_(default_accamp), accfrq_(default_accfrq),
			xinit_(default_xinit), vinit_(default_vinit) {}

		LinearOscillatorBuilder& SetOmega(const double omega) { this->omega_ = omega; return *this; }
		LinearOscillatorBuilder& SetZeta(const double zeta) { this->zeta_ = zeta; return *this; }
		LinearOscillatorBuilder& SetAccAmp(const double accamp) { this->accamp_ = accamp; return *this; }
		LinearOscillatorBuilder& SetAccFrq(const double accfrq) { this->accfrq_ = accfrq; return *this; }
		LinearOscillatorBuilder& SetXInit(const double xinit) { this->xinit_ = xinit; return *this; }
		LinearOscillatorBuilder& SetVInit(const double vinit) { this->vinit_ = vinit; return *this; }

		LinearOscillator Build()
		{
			// Check if certain values are in the right range
			try
			{
				if (this->zeta_ < 0.0) throw std::logic_error("Zeta should be positive.");
				if (this->omega_ < 0.0) throw std::logic_error("Omega should be positive");
				if (this->accfrq_ < 0.0) throw std::logic_error("Omega should be positive");
			}
			catch (std::logic_error& e)
			{
				std::cerr << e.what() << std::endl;
				exit(1);
			}

			return LinearOscillator(this->omega_, this->zeta_, this->accamp_, this->accfrq_, this->xinit_, this->vinit_);
		}
	};
}