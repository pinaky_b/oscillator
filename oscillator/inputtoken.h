#pragma once
#include <string>
#include <iostream>
#include <fstream>

namespace sdof
{
	template <typename T>
	class InputToken
	{
	private:
		std::string message_;
		T value_;
	public:
		InputToken(std::string const &message) { this->message_ = message; }
		InputToken& Execute() { std::cout << message_ << std::endl; std::cin >> value_; return *this; }
		InputToken& Execute(std::istream &is, std::ostream &os) { os << message_ << std::endl; is >> value_; return *this; }
		T GetValue() { return value_; }
	};
}