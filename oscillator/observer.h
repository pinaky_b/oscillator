#pragma once
#include <vector>
#include "oscillator.h"

namespace sdof
{
	struct Observer
	{
		std::vector< state_type >& m_states;
		std::vector< double >& m_times;

		Observer(std::vector< state_type > &states, std::vector< double > &times)
			: m_states(states), m_times(times) { }

		void operator()(const state_type &x, double t)
		{
			m_times.push_back(t);
			m_states.push_back(x);
		}
	};
}