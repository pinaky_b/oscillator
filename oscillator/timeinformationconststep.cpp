#include "timeinformationconststep.h"

namespace sdof
{
	const uint64_t TimeInformationConstStep::TimeInformationConstStepBuilder::default_num_timesteps = 100;
	const double TimeInformationConstStep::TimeInformationConstStepBuilder::default_dt = 0.01;
}