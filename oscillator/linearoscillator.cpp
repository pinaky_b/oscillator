#include "linearoscillator.h"
#include "boost/numeric/odeint.hpp"
#include "observer.h"

namespace sdof
{

	const double LinearOscillator::LinearOscillatorBuilder::default_omega = 1.0;
	const double LinearOscillator::LinearOscillatorBuilder::default_zeta = 0.1;
	const double LinearOscillator::LinearOscillatorBuilder::default_accamp = 1.0;
	const double LinearOscillator::LinearOscillatorBuilder::default_accfrq = 1.1;
	const double LinearOscillator::LinearOscillatorBuilder::default_xinit = 1.0;
	const double LinearOscillator::LinearOscillatorBuilder::default_vinit = 0.0;

	LinearOscillator::LinearOscillator()
	{
		this->omega_ = LinearOscillator::LinearOscillatorBuilder::default_omega;
		this->zeta_ = LinearOscillator::LinearOscillatorBuilder::default_zeta;
		this->accamp_ = LinearOscillator::LinearOscillatorBuilder::default_accamp;
		this->accfrq_ = LinearOscillator::LinearOscillatorBuilder::default_accfrq;
		this->xinit_ = LinearOscillator::LinearOscillatorBuilder::default_xinit;
		this->vinit_ = LinearOscillator::LinearOscillatorBuilder::default_vinit;
	}

	void LinearOscillator::Integrate(TimeInformationConstStep const &t)
	{
		
		const double dt = t.GetDt();
		const uint64_t num_timesteps = t.GetNumTimeSteps();
		const double tbegin = 0.0;
		const double tfinal = ((double)num_timesteps - 1.0) * dt;


		// Populate the initial state
		state_type initial_state(3);
		initial_state[0] = this->xinit_;
		initial_state[1] = this->vinit_;
		initial_state[2] = 0.0;

		// Reserve memory
		this->times.reserve(num_timesteps);
		this->x_vec.reserve(num_timesteps);

		// Perform the integration
		boost::numeric::odeint::runge_kutta4<state_type> stepper;
		size_t steps = boost::numeric::odeint::integrate_const(stepper, *this, initial_state, tbegin, tfinal, dt, Observer(this->x_vec, this->times));

	}

	void LinearOscillator::operator()(state_type const &x, state_type &dxdt, double t)
	{
		const double damping = 2.0 * (this->zeta_) * (this->omega_);
		const double omegasq = (this->omega_) * (this->omega_);
		const double accamp = this->accamp_;
		const double accfrq = this->accfrq_;
		dxdt[0] = x[1];
		dxdt[1] = -omegasq * x[0] - damping * x[1] + accamp * cos(x[2]);
		dxdt[2] = accfrq;
	}

	void LinearOscillator::PrintTrajectoryJSON(std::ostream &os)
	{
		uint64_t i, n;
		os << "\t\"trajectory\": [" << std::endl;

		try
		{
			std::string err_message("Oscillator times and x_vec have different sizes.Problem with integration / observation routine");
			if (!(this->times.size() == this->x_vec.size())) throw std::logic_error(err_message);
		}
		catch (std::exception &e)
		{
			std::cerr << e.what() << std::endl;
			exit(1);
		}

		for (n = 0; n < this->times.size() - 1; n++)
		{
			os << "\t\t[";
			os << times[n] << ",";
			for (i = 0; i < x_vec[n].size() - 1; i++)
				os << x_vec[n][i] << ",";
			os << x_vec[n][i];
			os << "]," << std::endl;
		}
		os << "\t\t[";
		os << times[n] << ",";
		for (i = 0; i < x_vec[n].size() - 1; i++)
			os << x_vec[n][i] << ",";
		os << x_vec[n][i];
		os << "]" << std::endl;

		os << "\t]" << std::endl;
	}

	void LinearOscillator::PrintParametersJSON(std::ostream &os, std::string const &s)
	{
		os << "\t\"" + s + "\": " << std::endl;
		os << "\t{" << std::endl;
		os << "\t\t\"omega\": " << this->omega_;
		os << ", \"zeta\": " << this->zeta_;
		os << ", \"accamp\": " << this->accamp_ << ", " << std::endl;
		os << "\t\t\"accfrq\": " << this->accfrq_;
		os << ", \"xinit\": " << this->xinit_;
		os << ", \"vinit\": " << this->vinit_ << std::endl;
		os << "\t}" << std::endl;
	}
}